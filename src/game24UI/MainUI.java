package game24UI;

import game24.Client;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.IOException;

import javax.swing.BoxLayout;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JTextField;

public class MainUI {
	public Client client;
	private MainUI _this;
	private GameUI game;
	public MainUI(){
		init();
		client = new Client();
		System.out.println(client.hashCode());
		_this = this;
	}
	public void init(){
		final JFrame frame = new JFrame();
		JLabel label = new JLabel("Please enter a name");
		final JTextField txt = new JTextField(15);
		JButton ok = new JButton("OK");
		JPanel pane = new JPanel();
		pane.setLayout(new BoxLayout(pane,BoxLayout.Y_AXIS));
		
		ok.addActionListener(new ActionListener(){
			public void actionPerformed(ActionEvent e) {
				if(txt.getText().equals(""))
					JOptionPane.showMessageDialog(null,"Please sign your name.","Error",JOptionPane.WARNING_MESSAGE);
				else{
					connectToServer();
					client.setUserName(txt.getText());
					frame.setVisible(false);
					game = new GameUI(_this);
					game.frame.setVisible(true);
				}
			}
		});
		
		pane.add(label);
		pane.add(txt);
		pane.add(ok);
		frame.add(pane);
		frame.setVisible(true);
		frame.pack();
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
	}
	public void connectToServer(){
		try {
			client.openConnection();
		} catch (IOException e1) {
			e1.printStackTrace();
		}
	}
}
