package game24UI;

import game24.Client;
import game24.numberGenerator;

import java.awt.BorderLayout;
import java.awt.FlowLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.util.ArrayList;
import java.util.List;
import java.util.Observable;
import java.util.Observer;

import javax.swing.BoxLayout;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JList;
import javax.swing.JPanel;
import javax.swing.JScrollPane;

public class GameUI implements Observer{
	JLabel number1,numSelect1,operSelect1;
	JLabel number2,numSelect2,operSelect2;
	JLabel number3,numSelect3,operSelect3;
	JLabel number4,numSelect4,Score,Score2;
	JLabel ansLabel;
	JList listPlayer;
	JLabel operPlus,operMinus,operMultiply,operDivide;
	JLabel [] numSelects = new JLabel [4];
	JLabel [] operSelects = new JLabel [3];
	ImageIcon[] icon = new ImageIcon [9];
	ImageIcon[] scoreicon = new ImageIcon[10];
	ImageIcon[] plainicon = new ImageIcon[2];
	ImageIcon[] opericon = new ImageIcon[4];
	ImageIcon[] etcicon = new ImageIcon[4];
	numberGenerator game;
	List<Integer> numList;
	List<String> operList;
	List<Client> clientList;
	JFrame frame;
	MainUI ui;
	public boolean check = false;
	public GameUI(MainUI ui){
		game = new numberGenerator(this);
		this.ui = ui;
		clientList = new ArrayList<Client>();
		System.out.println(ui.client.hashCode());
		init();
	}
	public void init(){
		frame = new JFrame();
		icon[0] = new ImageIcon(getClass().getResource("/images/1.jpg"));
		icon[1] = new ImageIcon(getClass().getResource("/images/2.jpg"));
		icon[2] = new ImageIcon(getClass().getResource("/images/3.jpg"));
		icon[3] = new ImageIcon(getClass().getResource("/images/4.jpg"));
		icon[4] = new ImageIcon(getClass().getResource("/images/5.jpg"));
		icon[5] = new ImageIcon(getClass().getResource("/images/6.jpg"));
		icon[6] = new ImageIcon(getClass().getResource("/images/7.jpg"));
		icon[7] = new ImageIcon(getClass().getResource("/images/8.jpg"));
		icon[8] = new ImageIcon(getClass().getResource("/images/9.jpg"));
		scoreicon[0] = new ImageIcon(getClass().getResource("/images/s0.jpg"));
		scoreicon[1] = new ImageIcon(getClass().getResource("/images/s1.jpg"));
		scoreicon[2] = new ImageIcon(getClass().getResource("/images/s2.jpg"));
		scoreicon[3] = new ImageIcon(getClass().getResource("/images/s3.jpg"));
		scoreicon[4] = new ImageIcon(getClass().getResource("/images/s4.jpg"));
		scoreicon[5] = new ImageIcon(getClass().getResource("/images/s5.jpg"));
		scoreicon[6] = new ImageIcon(getClass().getResource("/images/s6.jpg"));
		scoreicon[7] = new ImageIcon(getClass().getResource("/images/s7.jpg"));
		scoreicon[8] = new ImageIcon(getClass().getResource("/images/s8.jpg"));
		scoreicon[9] = new ImageIcon(getClass().getResource("/images/s9.jpg"));
		plainicon[0] = new ImageIcon(getClass().getResource("/images/s.jpg"));
		plainicon[1] = new ImageIcon(getClass().getResource("/images/w.jpg"));
		opericon[0] = new ImageIcon(getClass().getResource("/images/plus.jpg"));
		opericon[1] = new ImageIcon(getClass().getResource("/images/min.jpg"));
		opericon[2] = new ImageIcon(getClass().getResource("/images/mul.jpg"));
		opericon[3] = new ImageIcon(getClass().getResource("/images/div.jpg"));
		etcicon[0] = new ImageIcon(getClass().getResource("/images/correct.jpg"));
		etcicon[1] = new ImageIcon(getClass().getResource("/images/wrong.jpg"));
		etcicon[2] = new ImageIcon(getClass().getResource("/images/score.jpg"));
		etcicon[3] = new ImageIcon(getClass().getResource("/images/ans.jpg"));
		JPanel pane = new JPanel();
		pane.setLayout(new BoxLayout(pane, BoxLayout.Y_AXIS));
		JPanel topPane = new JPanel();
		topPane.setLayout(new FlowLayout());
		JPanel btmPane = new JPanel();
		btmPane.setLayout(new FlowLayout());
		JPanel first2Pane = new JPanel();
		first2Pane.setLayout(new FlowLayout());
		JPanel second2Pane = new JPanel();
		second2Pane.setLayout(new FlowLayout());
		JPanel operPane = new JPanel();
		operPane.setLayout(new FlowLayout());
		JPanel borderPane = new JPanel();
		borderPane.setLayout(new BorderLayout());
		JPanel compPane = new JPanel();
		compPane.setLayout(new BoxLayout(compPane, BoxLayout.Y_AXIS));
		JPanel topRightPane = new JPanel();
		topRightPane.setLayout(new BoxLayout(topRightPane, BoxLayout.Y_AXIS));
		JPanel scorePane = new JPanel();
		scorePane.setLayout(new FlowLayout());
		JPanel AnsPane = new JPanel();
		AnsPane.setLayout(new FlowLayout());
		numList = new ArrayList<Integer>();
		operList = new ArrayList<String>();
		numSelect1 = new JLabel();
		numSelect2 = new JLabel();
		numSelect3 = new JLabel();
		numSelect4 = new JLabel();
		operSelect1 = new JLabel();
		operSelect2 = new JLabel();
		operSelect3 = new JLabel();
		JLabel ScoreText = new JLabel();
		ScoreText.setIcon(etcicon[2]);
		Score = new JLabel();
		Score2 = new JLabel();
		Score.setIcon(scoreicon[0]);
		Score2.setIcon(scoreicon[0]);
		number1 = new JLabel();
		number1.addMouseListener(new MouseAdapter(){
			public void mouseClicked(MouseEvent arg0) {
				int num = game.getTemp(0);
				if(!numList.contains(num))
					selectNumber(num);
			}
		});
		number2 = new JLabel();
		number2.addMouseListener(new MouseAdapter(){
			public void mouseClicked(MouseEvent arg0) {
				int num = game.getTemp(1);
				if(!numList.contains(num))
					selectNumber(num);
			}
		});
		number3 = new JLabel();
		number3.addMouseListener(new MouseAdapter(){
			public void mouseClicked(MouseEvent arg0) {
				int num = game.getTemp(2);
				if(!numList.contains(num))
					selectNumber(num);
			}
		});
		number4 = new JLabel();
		number4.addMouseListener(new MouseAdapter(){
			public void mouseClicked(MouseEvent arg0) {
				int num = game.getTemp(3);
				if(!numList.contains(num))
					selectNumber(num);
			}
		});
		JLabel number24 = new JLabel();
		number24.setIcon(new ImageIcon(getClass().getResource("/images/make24.jpg")));
		operPlus = new JLabel();
		operPlus.setIcon(opericon[0]);
		operPlus.addMouseListener(new MouseAdapter(){
			public void mouseClicked(MouseEvent arg0) {
				if(!isAllSelect())
					selectOper(opericon[0],"+");
			}
		});
		operMinus = new JLabel();
		operMinus.setIcon(opericon[1]);
		operMinus.addMouseListener(new MouseAdapter(){
			public void mouseClicked(MouseEvent arg0) {
				if(!isAllSelect())
					selectOper(opericon[1],"-");
			}
		});
		operMultiply = new JLabel();
		operMultiply.setIcon(opericon[2]);
		operMultiply.addMouseListener(new MouseAdapter(){
			public void mouseClicked(MouseEvent arg0) {
				if(!isAllSelect())
					selectOper(opericon[2],"*");
			}
		});
		operDivide = new JLabel();
		operDivide.setIcon(opericon[3]);
		operDivide.addMouseListener(new MouseAdapter(){
			public void mouseClicked(MouseEvent arg0) {
				if(!isAllSelect())
					selectOper(opericon[3],"/");
			}
		});
		ansLabel = new JLabel();
		ansLabel.setIcon(new ImageIcon(getClass().getResource("/images/answer.jpg")));
		JButton Ans = new JButton();
		Ans.setIcon(etcicon[3]);
		Ans.addActionListener(new ActionListener(){
			public void actionPerformed(ActionEvent arg0) {
				if(isAllSelect()){
					int[] arrNum = new int [4];
					for(int i = 0;i < arrNum.length;i++){
						arrNum[i] = numList.get(i);
					}
					String[] arrOper = new String [3];
					for(int i = 0;i < arrOper.length;i++){
						arrOper[i] = operList.get(i);
					}
					if(game.check(arrNum,arrOper))
						ansLabel.setIcon(etcicon[0]);
					else
						ansLabel.setIcon(etcicon[1]);
				}
				else
					ansLabel.setIcon(etcicon[1]);
			}
		});
		listPlayer = new JList();
		clientList = ui.client.getPlayerList();
		listPlayer.setListData(clientList.toArray());
		JScrollPane scrollPane = new JScrollPane(listPlayer);
		numSelects[0] = numSelect1;
		numSelects[1] = numSelect2;
		numSelects[2] = numSelect3;
		numSelects[3] = numSelect4;
		operSelects[0] = operSelect1;
		operSelects[1] = operSelect2;
		operSelects[2] = operSelect3;
		clearSelects();
		setNumber();
		first2Pane.add(number1);
		first2Pane.add(number2);
		second2Pane.add(number3);
		second2Pane.add(number4);
		operPane.add(operPlus);
		operPane.add(operMinus);
		operPane.add(operMultiply);
		operPane.add(operDivide);
		borderPane.add(first2Pane,BorderLayout.NORTH);
		borderPane.add(number24,BorderLayout.CENTER);
		borderPane.add(second2Pane,BorderLayout.SOUTH);
		compPane.add(borderPane);
		compPane.add(operPane);
		scorePane.add(ScoreText);
		scorePane.add(Score2);
		scorePane.add(Score);
		topRightPane.add(scorePane);
		topRightPane.add(scrollPane);
		AnsPane.add(Ans);
		AnsPane.add(ansLabel);
		topRightPane.add(AnsPane);
		topPane.add(compPane);
		topPane.add(topRightPane);
		btmPane.add(numSelect1);
		btmPane.add(operSelect1);
		btmPane.add(numSelect2);
		btmPane.add(operSelect2);
		btmPane.add(numSelect3);
		btmPane.add(operSelect3);
		btmPane.add(numSelect4);
		
		pane.add(topPane);
		pane.add(btmPane);

		frame.add(pane);
		frame.pack();
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
	}
	public void selectNumber(int num){
		numList.add(num);
		numSelects[numList.size()-1].setIcon(icon[num-1]);
	}
	public void selectOper(ImageIcon op,String oper){
		operList.add(oper);
		operSelects[operList.size()-1].setIcon(op);
	}
	public boolean isAllSelect(){
		if(numList.size() == 4 && operList.size() == 3)
			return true;
		return false;
	}
	public void setNumber(){
		game.problem();
		number1.setIcon(icon[game.getTemp(0)-1]);
		number2.setIcon(icon[game.getTemp(1)-1]);
		number3.setIcon(icon[game.getTemp(2)-1]);
		number4.setIcon(icon[game.getTemp(3)-1]);
	}
	public void clearSelects(){
		numSelect1.setIcon(plainicon[0]);
		numSelect2.setIcon(plainicon[0]);
		numSelect3.setIcon(plainicon[0]);
		numSelect4.setIcon(plainicon[0]);
		operSelect1.setIcon(plainicon[1]);
		operSelect2.setIcon(plainicon[1]);
		operSelect3.setIcon(plainicon[1]);
	}
//	public static void main(String [] args){
//		GameUI ui = new GameUI();
//	}
	public void update(Observable game, Object arg1) {
		if(check){
			setNumber();
			ui.client.score++;
			ui.client.updateScore();
			Score.setIcon(scoreicon[ui.client.score%10]);
			Score2.setIcon(scoreicon[ui.client.score/10]);
			clientList = ui.client.getPlayerList();
			listPlayer.setListData(clientList.toArray());
		}
		System.out.println("clear");
		numList.clear();
		operList.clear();
		clearSelects();
		ansLabel.setText("");
	}
}
