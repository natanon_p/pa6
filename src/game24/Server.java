package game24;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.lloseng.ocsf.server.AbstractServer;
import com.lloseng.ocsf.server.ConnectionToClient;
import com.lloseng.ocsf.server.ObservableServer;

public class Server extends AbstractServer{
	private Map<Integer,ConnectionToClient> clientList = new HashMap<Integer,ConnectionToClient>();
	private static final int PORT = 5000;
	public Server(){
		super(PORT);
	}
	protected synchronized void clientConnected(ConnectionToClient client) {
		clientList.put(clientList.size(),client);
		client.setInfo("score", 0);
	}
	public void handleMessageFromClient(Object msg, ConnectionToClient client) {
		String message = (String)msg;
		System.out.println(message);
		if(message.substring(0, 3).equals("Add")){
			System.out.println("Adding");
			client.setInfo("name", message.substring(4));
			sentPlayerListToClients();
		}
		else if(message.equals("Get")){
			try {
				client.sendToClient("Give "+ client.getInfo("name"));
			} catch (IOException e) {
				e.printStackTrace();
			}
		}
		else if(message.substring(0, 6).equals("Update")){
			int score = Integer.parseInt(message.substring(6).trim());
			client.setInfo("score", score);
			sentPlayerListToClients();
		}
	}
	public void sentPlayerListToClients(){
		System.out.println(clientList.size());
		for(int i = 0;i < clientList.size();i++){
			System.out.println("Sending");
			super.sendToAllClients("New " + clientList.get(i).getInfo("name") + "," + clientList.get(i).getInfo("score"));
		}
	}
	public static void main(String [] args){
		Server server = new Server();
		try {
			server.listen();
		} catch (IOException e) {
			System.out.println("Couldn't start server:");
			System.out.println(e);
		}
	}
}
