package game24;

import game24UI.GameUI;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;
import java.util.Observable;
import java.util.Random;
import java.util.Scanner;

public class numberGenerator extends Observable{
	private final int ANS = 24;
	private final String[] OPERATOR = {"+","-","*","/"};
	private List<String> numberSetList = new ArrayList<String>();
	private int [] temp = new int [4];
	private GameUI ui;
	public numberGenerator(GameUI ui){
		try{
			InputStream stream = getClass().getResourceAsStream("/text24/24.txt");
			InputStreamReader input = new InputStreamReader(stream);
			BufferedReader bufferReader = new BufferedReader(input);
			while(true){
				String set = bufferReader.readLine();
				if(set == null)
					break;
				numberSetList.add(set);
			}
		}catch(IOException e){
			e.printStackTrace();
		}
		this.ui = ui;
		this.addObserver(ui);
	}

	public int[] problem(){
		int problemNo = (int) Math.floor((Math.random() * 122) + 1 );
		String problem = numberSetList.get(problemNo);
		String[] arr = problem.split(",");
		for(int i = 0; i < arr.length; i++){
			temp[i] = Integer.parseInt(arr[i]);
		}
		return temp;
	}

	public boolean check(int[] num,String[] oper){
		if(applyOperator(applyOperator(applyOperator(num[0],num[1],oper[0]),num[2],oper[1]),num[3],oper[2]) == 24){
			ui.check = true;
			super.setChanged();
			super.notifyObservers(this);
			return true;
		}
		else if(applyOperator(applyOperator(num[0],num[1],oper[0]),applyOperator(num[2],num[3],oper[2]),oper[1]) == 24){
			ui.check = true;
			super.setChanged();
			super.notifyObservers(this);
			return true;
		}
		else if(applyOperator(num[0],applyOperator(num[1],applyOperator(num[2],num[3],oper[0]),oper[1]),oper[2]) == 24){
			ui.check = true;
			super.setChanged();
			super.notifyObservers(this);
			return true;
		}
		else if(applyOperator(applyOperator(num[0],applyOperator(num[1],num[2],oper[1]),oper[0]),num[3],oper[2]) == 24){
			ui.check = true;
			super.setChanged();
			super.notifyObservers(this);
			return true;
		}
		System.out.println("Notify");
		ui.check = false;
		super.setChanged();
		super.notifyObservers(this);
		return false;
	}

	public int applyOperator(int num1,int num2,String oper){
		if(oper.equals("+"))
			return num1 + num2;
		else if(oper.equals("-"))
			return num1 - num2;
		else if(oper.equals("*"))
			return num1 * num2;
		else if(oper.equals("/"))
			return num1 / num2;
		return 0;
	}
	
	public int getTemp(int index){
		int number = temp[index];
		return number;
	}

}
