package game24;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Observer;

import com.lloseng.ocsf.client.AbstractClient;
import com.lloseng.ocsf.server.ConnectionToClient;

public class Client extends AbstractClient{
	public String username;
	public List<Client> list;
	public int score = 0;
	public Client(){
		super("localhost",5000);
		list = new ArrayList<Client>();
	}

	public void handleMessageFromServer(Object obj) {
		String msg = (String)obj;
		System.out.println(msg);
		if(msg.substring(0, 3).equals("New")){
			Client client = new Client();
			String[] info = msg.substring(3).trim().split(",");
			client.username = info[0];
			if(!list.contains(client))
				list.add(client);
			if(list.contains(client)){
				list.get(list.indexOf(client)).setScore(Integer.parseInt(info[1]));
				System.out.println(list.get(list.indexOf(client)).score);
			}
		}
		if(msg.substring(0, 4).equals("Give")){
			username = msg.substring(4).trim();
		}
	}
	public void updateScore(){
		try {
			this.sendToServer("Update "+ this.score);
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
	public void setScore(int score){
		this.score = score;
	}
	public void setUserName(String name){
		try {
			this.sendToServer("Add "+name);
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
	public void getUserName(){
		try {
			this.sendToServer("Get");
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
	public List<Client> getPlayerList(){
		System.out.println(list.toArray().toString());
		return list;
	}
	public boolean equals(Client client){
		if(this.username.equals(client.username))
			return true;
		return false;
	}
	public String toString(){
		return username + " Solved : " + score;
	}
}
